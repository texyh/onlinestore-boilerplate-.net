﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using OnlineStore.Core.Models;

namespace OnlineStore.Core.Interfaces
{
    public interface IProductService
    {
        Task<IEnumerable<Product>> GetAllProducts();

        Task<Product> GetProduct(Guid id);

        Task<IEnumerable<Product>> GetByCategory(Category category);
    }
}