﻿using System;

namespace OnlineStore.Core.Models
{
    public class Product
    {
        public Guid Id { get; set; }

        public string  Name { get; set; }

        public string Description { get; set; }

        public string Price { get; set; }

        public Category Category { get; set; }


    }
}