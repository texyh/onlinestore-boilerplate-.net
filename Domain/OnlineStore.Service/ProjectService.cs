﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OnlineStore.Core.Models;
using OnlineStore.Core.Interfaces;
using OnlineStore.Core;

namespace OnlineStore.Service
{
    public class ProductService : IProductService
    {
        public async  Task<IEnumerable<Product>> GetAllProducts()
        {
            // Todo pass an task function
            return new List<Product>();
        }

        public async Task<Product> GetProduct(Guid id)
        {
            // Todo pass an task function
            return  new Product();
        }

        public async Task<IEnumerable<Product>> GetByCategory(Category category)
        {
            // Todo pass an task function
            return new List<Product>();
        }
    }
}